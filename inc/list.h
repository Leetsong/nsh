#ifndef __LIST_H__
#define __LIST_H__

#ifndef NULL
#define NULL ((void*) 0)
#endif

typedef struct list list;

struct list {
	struct list *next, *prev;
};

static inline void list_init(struct list *list) {
	list->next = list;
	list->prev = list;
}

static inline int list_empty(const struct list *list) {
	return list->next == list;
}

static inline void list_insert(struct list *link, struct list *new_link) {
	new_link->prev		= link->prev;
	new_link->next		= link;
	new_link->prev->next	= new_link;
	new_link->next->prev	= new_link;
}

static inline void list_append(struct list *list, struct list *new_link) {
	list_insert((struct list *)list, new_link);
}

static inline void list_prepend(struct list *list, struct list *new_link) {
	list_insert(list->next, new_link);
}

static inline void list_remove(struct list *link) {
	link->prev->next = link->next;
	link->next->prev = link->prev;
}

static inline int list_length(const struct list *list, int pos) {
	int length = 0;

	for (struct list* p = list->next; p != list; p = p->next) {
		length ++;
	}

	return length;
}

static inline struct list* list_get(const struct list *list, int pos) {
	int i = 0;
	
	for (struct list* p = list->next; p != list; p = p->next, i ++) {
		if(i == pos) {
			return p;
		}
	}

	return NULL;
}

#define list_entry(link, type, member) \
	((type *)((char *)(link)-(unsigned long)(&((type *)0)->member)))

#define list_head(list, type, member)		\
	list_entry((list)->next, type, member)

#define list_tail(list, type, member)		\
	list_entry((list)->prev, type, member)

#define list_next(elm, member)					\
	list_entry((elm)->member.next, typeof(*elm), member)

#define list_for_each(elm, list)			\
	for (elm = list->next; elm != (list);	elm = elm->next)

#define list_for_each_index(elm, index, list)			\
	for (elm = list->next, index = 0; elm != (list);	elm = elm->next, index ++)

#define list_for_each_entry(elm, list, member)			\
	for (elm = list_head(list, typeof(*elm), member);	\
	     &elm->member != (list);				\
	     elm = list_next(elm, member))

#endif