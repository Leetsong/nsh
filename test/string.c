#include <stdio.h>
#include "../inc/strfn.h"

int main() {
	char s[3]; s[0] = '0'; s[1] = '0'; s[2] = '0';
	printf("%s", strtrim(s));
	// printf("%s", strtrim(" 123"));
	// // printf("%s", strtrim("123 "));
	// // printf("%s", strtrim("123\t"));
	// // printf("%s", strtrim("\t\n 123"));
	// // printf("%s", strtrim("   123\n\t"));
	// // printf("%s", strtrim("\t\n 123\t\n "));

	return 0;
}