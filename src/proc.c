#include "common.h"
#include "logger.h"
#include "strfn.h"
#include "list.h"
#include "proc.h"

Proc* proc_create() {
	Proc* proc = (Proc*) malloc(sizeof(Proc));
	memset(proc, 0, sizeof(Proc));
	list_init(&proc->neighbors);
	return proc;
}


Proc* proc_create_from_string(const char* string) {
	Proc* proc = proc_create();
	char** argv = (char**) strsplit(string, " \t\n");

	proc->bin = argv[0];
	proc->argv = argv;

	for(int i = 0; argv[i]; i ++) {
		proc->argc ++;
	}

	return proc;
}

void proc_destroy(Proc* proc) {
	for(int i = 0; proc->argv[i]; i ++) {
		free(proc->argv[i]);
	}

	free(proc);
}