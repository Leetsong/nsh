#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <assert.h>

#ifdef __NSH_DEBUG__

#define Assert(cond, ...) do { \
	if(!(cond)) { \
		fflush(stdout); \
		fprintf(stderr, "\33[1;31m"); \
		fprintf(stderr, __VA_ARGS__); \
		fprintf(stderr, "\33[0m\n"); \
		assert(cond); \
	} \
} while(0)

#define panic(format, ...) \
	Assert(0, format, ## __VA_ARGS__)

#else

#define Assert(cond, ...) \
	do {} while(0)

#define panic(format, ...) \
	do {} while(0)

#endif

#endif