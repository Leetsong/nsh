#include <time.h>
#include "common.h"
#include "logger.h"
#include "debug.h"
#include "strfn.h"
#include "proc.h"
#include "nsh.h"

static int _nsh_internal_cmd_exit(Nsh* instance, const char* argv[]);
static int _nsh_internal_cmd_cd(Nsh* instance, const char* argv[]);
static int _nsh_internal_cmd_pwd(Nsh* instance, const char* argv[]);

static bool g_nsh_has_initialized = false;
static Nsh g_nsh;
static NshInternalCmd g_nsh_internal_cmds[] = {
	{ "exit", &_nsh_internal_cmd_exit },
	{ "cd", &_nsh_internal_cmd_cd },
	{ "pwd", &_nsh_internal_cmd_pwd }
};

#define NSH_NR_INTERNAL_CMDS \
	sizeof(g_nsh_internal_cmds) / sizeof(NshInternalCmd)

// global

Nsh* get_nsh() {
	if(g_nsh_has_initialized == false) {
		g_nsh.has_err = false;
		g_nsh.env_path = strsplit(getenv("PATH"), ":");
		strcpy(g_nsh.user, getlogin());
		getcwd(g_nsh.pwd, NSH_PATHNAME_MAX_LEN);
		gethostname(g_nsh.host, NSH_HOST_MAX_LEN);
		g_nsh_has_initialized = true;
	}

	return &g_nsh;
}

bool is_internal_cmd(const char* bin) {
	return get_internal_cmd(bin) != -1;
}

bool is_external_cmd(const char* bin) {
	// absolute
	if (strstwith(bin, "/") || strstwith(bin, "./") || strstwith(bin, "../")) {
		return access(bin, X_OK) == 0;
	}

	// environment
	char bin_absolute_path[NSH_PATHNAME_MAX_LEN];
	for(int i = 0; g_nsh.env_path[i]; i ++) {
		strcpy(bin_absolute_path, g_nsh.env_path[i]);
		strcat(bin_absolute_path, "/");
		strcat(bin_absolute_path, bin);
		if(access(bin_absolute_path, X_OK) == 0) {
			return true;
		}
	}

	return false;
}

int get_internal_cmd(const char* bin) {
	for(int i = 0; i < NSH_NR_INTERNAL_CMDS; i ++) {
		if(strcmp(bin, g_nsh_internal_cmds[i].name) == 0) {
			return i;
		}
	}

	return -1;
}

// nsh

int nsh_run(Nsh* self) {
	char * input;
	NshParseInfo* parse_info;

	do {
		input = nsh_prompt(self);
		parse_info = nsh_parse(self, input);
		nsh_process(self, parse_info);
		nsh_parse_info_destroy(self, parse_info);
	} while(1);

	return 0;
}

/**
 * DANGEROUS: input can only used only once, please copy
 * it to other places
 */
char* nsh_prompt(Nsh* self) {
	// we hold the input in the static area
	static char input[NSH_INPUT_MAX_LEN + 1];
	time_t current_time;
	struct tm *t;

	time(&current_time);
	t = localtime(&current_time);

	logln(LOGGER_FMT_BOLD "%s" 
		LOGGER_FMT_NONE "@%s" 
		LOGGER_FMT_BOLD LOGGER_FMT_YELLOW " in " 
		LOGGER_FMT_NONE LOGGER_FMT_BOLD "%s " 
		LOGGER_FMT_NONE "[%02d:%02d:%02d]",
		g_nsh.user, g_nsh.host, g_nsh.pwd, t->tm_hour, t->tm_min, t->tm_sec);
	if(self->has_err) {
		log(LOGGER_FMT_RED "nsh$ ");
	} else {
		log(LOGGER_FMT_GREEN "nsh$ ");
	}
	
	fgets((char*) input, NSH_INPUT_MAX_LEN, stdin);
	input[NSH_INPUT_MAX_LEN] = '\0';

	// free(t);

	return strtrim(input);
}

NshParseInfo* nsh_parse(Nsh* self, char* input) {
	NshParseInfo* parse_info = nsh_parse_info_create_default(self);
	size_t len = strlen(input);

	if(strcmp(input, "") == 0) {
		return parse_info;
	}

	if(input[len - 1] == '&') {
		parse_info->running_ground = NSH_RUN_PROC_IN_BG;
		input[len - 1] = '\0';
		len -= 1;
	}

	if(strstr(input, "&&") != NULL) {
		panic("&& is not supported by now");
		parse_info->running_mode = NSH_RUN_ANDED_PROCS;
	}

	if(strstr(input, "||") != NULL) {
		panic("|| is not supported by now");
		parse_info->running_mode = NSH_RUN_ORED_PROCS;
	}

	if(strchr(input, '|') != NULL) {
		parse_info->running_mode = NSH_RUN_PIPED_PROCS;
		char** args = (char**) strsplit(input, "|");
		for(int i = 0; args[i]; i ++) {
			list_append(&parse_info->procs, 
				&proc_create_from_string(args[i])->neighbors);
			free(args[i]);
		}
		return parse_info;
	}

	// normal
	list_append(&parse_info->procs, 
		&proc_create_from_string(input)->neighbors);;

	return parse_info;
}

Nsh* nsh_process(Nsh* self, const NshParseInfo* parse_info) {
	if(list_empty(&parse_info->procs)) {
		return self;
	}

	switch(parse_info->running_mode) {
		case NSH_RUN_PIPED_PROCS:
			nsh_run_piped_procs(self, parse_info->running_ground, &parse_info->procs);
			break;
		case NSH_RUN_ANDED_PROCS:
			nsh_run_anded_procs(self, parse_info->running_ground, &parse_info->procs);
			break;
		case NSH_RUN_ORED_PROCS:
			nsh_run_ored_procs(self, parse_info->running_ground, &parse_info->procs);
			break;
		case NSH_RUN_NORM_PROCS:
			nsh_run_single_proc(self,
				parse_info->running_ground, 
				list_entry(parse_info->procs.next, Proc, neighbors));
			break;
		default:
			panic("Illegal running mode: %d(only piped, anded, ored, and norm are allowed", parse_info->running_mode);
	}
	
	return self;
}

Nsh* nsh_run_piped_procs(Nsh* self, int running_ground, const list* procs) {
	int to_close_fd = -1;
	list* p = NULL;

	list_for_each(p, procs) {
		Proc* proc = list_entry(p, Proc, neighbors);
		bool is_last_proc = (p->next == procs);

		pid_t pid;
		int fd[2];

		// the last one does not need to create pipe
		if(!is_last_proc && pipe(fd) == -1) {
			logln("nsh: failed in creating pipe");
			return self;
		}

		pid = fork();

		if(pid == 0) {
			// child, for not last one, we redirect out to fd[1]
			if(!is_last_proc) {
				close(fd[0]);
				dup2(fd[1], NSH_STDOUT);
			}

			// to_close_fd is used for this proc, this is reading from pipe
			if(to_close_fd != -1) {
				dup2(to_close_fd, NSH_STDIN);
			}

			execvp(proc->bin, proc->argv);
		} else {
			// parent, we close the previous opened fd(read from pipe)
			if(to_close_fd != -1) {
				close(to_close_fd);
			}

			// we close writing to pipe, not useful for the next proc,
			// but reading from pipe is useful for the next proc
			if(!is_last_proc) {
				close(fd[1]);
				to_close_fd = fd[0]; // we will close it in next round				
			}

			proc->pid = pid;
		}
	}

	if(running_ground == NSH_RUN_PROC_IN_FG) {
		list_for_each(p, procs) {
			waitpid(list_entry(p, Proc, neighbors)->pid, NULL, 0);
		}
	}
	g_nsh.has_err = false;

	return self;
}

Nsh* nsh_run_anded_procs(Nsh* self, int running_ground, const list* procs) {
	panic("&& is not supported by now");
	return self;
}

Nsh* nsh_run_ored_procs(Nsh* self, int running_ground, const list* procs) {
	panic("|| is not supported by now");
	return self;
}

Nsh* nsh_run_single_proc(Nsh* self, int running_ground, Proc* proc) {
	if(is_internal_cmd(proc->bin)) {
		nsh_run_internal_proc(self, running_ground, proc);
		return self;
	} else if(is_external_cmd(proc->bin)) {
		switch(running_ground) {
		case NSH_RUN_PROC_IN_BG: 
			nsh_run_external_proc_in_bg(self, proc);
			break;
		case NSH_RUN_PROC_IN_FG:
			nsh_run_external_proc_in_fg(self, proc);
			break;
		}
		return self;
	}

	logln("nsh:command not found: %s", proc->bin);
	self->has_err = true;

	return self;
}

Nsh* nsh_run_internal_proc(Nsh* self, int running_ground, Proc* proc) {
	int handler = get_internal_cmd(proc->bin);
	self->has_err = (g_nsh_internal_cmds[handler].handle(self, (const char**)proc->argv) != 0);
	return self;
}

Nsh* nsh_run_external_proc_in_bg(Nsh* self, Proc* proc) {
	if(fork() == 0) {
		// child
		execvp(proc->bin, proc->argv);
	} else {
		// parent do nothing
		self->has_err = false;
	}
	return self;
}

Nsh* nsh_run_external_proc_in_fg(Nsh* self, Proc* proc) {
	pid_t pid;
	int status;

	pid = fork();

	if(pid == 0) {
		// child
		execvp(proc->bin, proc->argv);
	} else {
		// parent wait until child over
		waitpid(pid, &status, 0);
		if(status != 0) {
			self->has_err = true;
		} else {
			self->has_err = false;
		}
	}
	return self;
}

// nsh parse info

NshParseInfo* nsh_parse_info_create_default(Nsh* self) {
	NshParseInfo* i = (NshParseInfo*) malloc(sizeof(NshParseInfo));

	memset(i, 0, sizeof(NshParseInfo));
	i->running_mode = NSH_RUN_NORM_PROCS;
	i->running_ground = NSH_RUN_PROC_IN_FG;
	list_init(&i->procs);

	return i;
}

void nsh_parse_info_destroy(Nsh* self, NshParseInfo* parse_info) {
	Proc* proc;

	list_for_each_entry(proc, &parse_info->procs, neighbors) {
		proc_destroy(proc);
	}

	free(parse_info);
}

// nsh internal commands

static int _nsh_internal_cmd_exit(Nsh* instance, const char* argv[]) {
	for(int i = 0; instance->env_path[i]; i ++) {
		free(instance->env_path[i]);
	}

	exit(0);

	return 0;
}

static int _nsh_internal_cmd_cd(Nsh* instance, const char* argv[]) {
	int ret = chdir(argv[1]);

	if(ret == 0) {
		getcwd(instance->pwd, NSH_PATHNAME_MAX_LEN);
	} else {
		logln("cd: no such file or directory: %s", argv[1]);
	}

	return ret;
}

static int _nsh_internal_cmd_pwd(Nsh* instance, const char* argv[]) {
	logln("%s", instance->pwd);
	return 0;
}
