#ifndef __NSH_NSH_H__
#define __NSH_NSH_H__

#include <stdbool.h>
#include "common.h"
#include "proc.h"
#include "list.h"

#define NSH_INPUT_MAX_LEN    256
#define NSH_USER_MAX_LEN     128
#define NSH_HOST_MAX_LEN     256
#define NSH_PATHNAME_MAX_LEN 1024

#define NSH_RUN_PROC_IN_FG 1
#define NSH_RUN_PROC_IN_BG 2

#define NSH_RUN_NORM_PROCS  0
#define NSH_RUN_PIPED_PROCS 1
#define NSH_RUN_ANDED_PROCS 2
#define NSH_RUN_ORED_PROCS  3

#define NSH_STDIN  0
#define NSH_STDOUT 1
#define NSH_STDERR 2

typedef struct NaiveShell Nsh;
typedef struct NshParseInfo NshParseInfo;
typedef struct NshInternalCmd NshInternalCmd;

// #define NSH_PARSE_INFO_SHOULD_EXIT 0x00000001
// #define NSH_PARSE_INFO_IS_INTERNAL 0x00000002

// nsh

struct NaiveShell {
	bool has_err;
	char user[NSH_USER_MAX_LEN];
	char host[NSH_HOST_MAX_LEN];
	char pwd[NSH_PATHNAME_MAX_LEN];
	char** env_path;
};

int nsh_run(Nsh* self);
NshParseInfo* nsh_parse(Nsh* self, char* input);
Nsh* nsh_process(Nsh* self, const NshParseInfo* parse_info);
Nsh* nsh_run_piped_procs(Nsh* self, int running_ground, const list* procs);
Nsh* nsh_run_anded_procs(Nsh* self, int running_ground, const list* procs);
Nsh* nsh_run_ored_procs(Nsh* self, int running_ground, const list* procs);
Nsh* nsh_run_single_proc(Nsh* self, int running_ground, Proc* proc);
Nsh* nsh_run_internal_proc(Nsh* self, int running_ground, Proc* proc);
Nsh* nsh_run_external_proc_in_fg(Nsh* self, Proc* proc);
Nsh* nsh_run_external_proc_in_bg(Nsh* self, Proc* proc);
char* nsh_prompt(Nsh* self);

// nsh parse info

struct NshParseInfo {
	int running_ground;
	int running_mode;
	list procs;
};

NshParseInfo* nsh_parse_info_create_default(Nsh* instance);
bool nsh_parse_info_should_pipe(Nsh* instance, NshParseInfo* parse_info);
void nsh_parse_info_destroy(Nsh* instance, NshParseInfo* parse_info);

struct NshInternalCmd {
	const char* name;
	int (*handle)(Nsh* instance, const char* argv[]);
};

// global

Nsh* get_nsh();
bool is_internal_cmd(const char* bin);
bool is_external_cmd(const char* bin);
int get_internal_cmd(const char* bin);

#endif