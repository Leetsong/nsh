#ifndef __STRING_H__
#define __STRING_H__

#include <string.h>
#include <stdbool.h>

char* strdup(const char* s);
char* strndup(const char* s, size_t l);
bool strstwith(const char* s, const char* st);
bool stredwith(const char* s, const char* ed);
char* strtrim(char* s);
char** strsplit(const char* s, const char* delimiter);
char* strjoin(const char** s, const char* delimiter);
char** strssplit(const char* s, const char* delimiter, size_t* const sz);
char* strsjoin(const char** s, size_t size, const char* delimiter);

#endif