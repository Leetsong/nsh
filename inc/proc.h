#ifndef __NSH_PROC_H__
#define __NSH_PROC_H__

#include "common.h"
#include "list.h"

typedef struct Process Proc;

struct Process {
	pid_t pid;
	char* bin;	// binary file, path are included in it
	int argc;
	char** argv; // arguments
	list neighbors;
};

Proc* proc_create();
Proc* proc_create_from_string(const char* string);
void proc_destroy(Proc* proc);

#endif