CC = gcc
CFLAGS = -Wall -Werror -I$(INC_DIR)

ifneq ($(__NSH_ENV__), __NSH_PROD__)
CFLAGS += -D__NSH_DEBUG__
endif

BIN_DIR = ./bin
SRC_DIR = ./src
INC_DIR = ./inc

SRC_FILES = $(shell find $(SRC_DIR) -name "*.c" -maxdepth 1)
INC_FILES = $(shell find $(INC_DIR) -name "*.h" -maxdepth 1)
OBJ_FILES = $(foreach f,$(notdir $(SRC_FILES)),$(BIN_DIR)/$(patsubst %.c,%.o,$(f)))

.PHONY: all dev build debug clean

# project building

prog = nsh

all: $(prog)

$(prog): $(OBJ_FILES)
	$(CC) -o $(prog) $(OBJ_FILES) $(CFLAGS)

$(OBJ_FILES): bin/%.o : src/%.c
	$(CC) -c $< $(CFLAGS) -o $@

# some scripts

build:
	__NSH_ENV__=__NSH_PROD__ make

dev:
	__NSH_ENV__=__NSH_DEV__ make

run:
	@./nsh

debug:
	make dev && gdb $(prog)

clean:
	-rm -f $(prog) $(OBJ_FILES)